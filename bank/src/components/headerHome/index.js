import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet
} from 'react-native';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ripple from 'react-native-material-ripple';
import ElevatedView from 'react-native-elevated-view';

export default class HeaderHome extends Component{
  constructor(props){
    super(props);
  }
  render(){
    return (
      <ElevatedView elevation={1} style={styles.container}>
      <View style={{flexDirection: 'row'}}>
      <Image
        style={{width: 30, height: 30}}
        source={require('../../imagens/logo1.png')}
      />
      <Text style={{fontSize:20,fontWeight:'bold', color:'#fff', marginLeft: 2}}>JC</Text>
      </View>
        <Ripple onPress={()=>this.props.funIcone()}>
            <MaterialIcons name="account-circle" size={30} color="#fff" />
        </Ripple>
      </ElevatedView>
    )
  }
}

const styles=StyleSheet.create({
  container:{
    flexDirection:'row',
    height:60,
    backgroundColor:'#A82BB8',
    padding:10,
    alignItems:'center',
    justifyContent:'space-between'
  }
})
