import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Actions} from 'react-native-router-flux';
import firebase from 'react-native-firebase';

import {fetchPosts, logarWithRedux, verificarLogado,singOut} from '../../Reducer';

import Feather from 'react-native-vector-icons/Feather';
import Ripple from 'react-native-material-ripple';
import ElevatedView from 'react-native-elevated-view';

class HeaderPerfil extends Component{
  constructor(props){
    super(props);
  }

  goSingOut(){
      firebase.auth().signOut().then(()=>{
        this.props.singOut();
        let setInter= setInterval(()=>{
          if (this.props.userDados==null) {
            clearInterval(setInter);
            Actions.Login({});
          }
        },100);
      }).catch((err)=>{
        console.log(err);
      });
  }

  render(){
    return (
      <ElevatedView elevation={1} style={styles.container}>
      <View style={{flexDirection: 'row'}}>
      <Image
        style={{width: 30, height: 30}}
        source={require('../../imagens/logo1.png')}
      />
      <Text style={{fontSize:20,fontWeight:'bold', color:'#fff', marginLeft: 2}}>JC</Text>
      </View>
        <Text style={{fontSize:20,fontWeight:'bold',alignSelf:'center', color:'#fff'}}>{this.props.nome}</Text>
        <Ripple onPress={()=>this.goSingOut()}>
            <Feather name="log-out" size={30} color="#fff" />
        </Ripple>
      </ElevatedView>
    )
  }
}

const styles=StyleSheet.create({
  container:{
    flexDirection:'row',
    height:60,
    backgroundColor:'#A82BB8',
    padding:10,
    alignItems:'center',
    justifyContent:'space-between'
  }
})

const mapStateToProps=(state)=>{
  return {userDados:state.user.userDados};
}

const mapDispatchToProps=(dispatch)=>{
  return bindActionCreators({fetchPosts, logarWithRedux,verificarLogado,singOut},dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(HeaderPerfil);
