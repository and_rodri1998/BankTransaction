import React, { Component} from 'react';
import { Router, Scene,Stack} from 'react-native-router-flux';

import Home from './screens/home';
import Login from './screens/login';
import SingUp from './screens/cadastro';
import SingComp from './screens/cadastro/SingComp';
import Perfil from './screens/Perfil';
import TelaDeposito from './screens/telaDeposito';
import TelaTransferencia from './screens/telaTransferencia';
import SwitchSenha from './screens/SwitchSenha';
import SenhaQuatro from './screens/SenhaQuatro';
import SenhaOito from './screens/SenhaOito';
import ExcluirConta from './screens/excluirConta';

export default class RouterComponent extends Component{
  render(){
    return (
      <Router style={{paddingTop:65}}>
        <Stack key="root">
          <Scene type="reset" key="Login" component={Login} title="DBank" hideNavBar={true} initial/>
          <Scene type="reset" key="Inicio" component={Home} title="Tela Inicial" hideNavBar={true} />
          <Scene key="Cadastro" component={SingUp} title="Cadastro" hideNavBar={true}/>
          <Scene key="CompCadastro" component={SingComp} title="Cadastro" hideNavBar={true}/>
          <Scene key="Perfil" component={Perfil} title="Perfil" hideNavBar={true}/>
          <Scene key="TelaDeposito" component={TelaDeposito} title="Tela de Depósito" hideNavBar={true} />
          <Scene key="TelaTransferencia" component={TelaTransferencia} title="Tela de Transferência" hideNavBar={true} />
          <Scene key="SwitchSenha" component={SwitchSenha} title="Qual senha alterar?" hideNavBar={true}/>
          <Scene key="SenhaQuatro" component={SenhaQuatro} title="Senha de 4 dígitos" hideNavBar={true} />
          <Scene key="SenhaOito" component={SenhaOito} title="Senha de 8 dígitos" hideNavBar={true} />
          <Scene key="ExcluirConta" component={ExcluirConta} title="xcluir Conta" hideNavBar={true} />
        </Stack>
      </Router>
    )
  }
}

// como passar de uma tela para outra
// import {Actions} from 'react-native-router-flux';
// Actions.Detalhes({idPost:id});
