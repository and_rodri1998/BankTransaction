import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {createStore, applyMiddleware} from 'redux';
import ReduxPromise from 'redux-promise';
import { Provider } from 'react-redux';

import reducers from './Reducer';

import Home from './screens/home';
import RouterComponent from './Router';

const createStoreWithMiddleware= applyMiddleware(ReduxPromise)(createStore);
const store = createStoreWithMiddleware(reducers);

export default class App extends Component<Props> {
  render() {
    return (
      <Provider store={store}>
        <RouterComponent />
      </Provider>
    );
  }
}
