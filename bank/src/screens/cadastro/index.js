import React,{ Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  ScrollView
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ripple from 'react-native-material-ripple';
import {Actions} from 'react-native-router-flux';

import firebase from 'react-native-firebase';

import HeaderPadrao from '../../components/headerPadrao';

export default class SingUp extends Component{
  constructor(props){
    super(props);
    this.state={
      email:'',
      senhaQuatro:'',
      senhaOito:'',
      nome:'',
      sobrenome:'',
      cpf:'',
      numTel:''
    }
  }

  continueCad(email,senhaQuatro,senhaOito,nome,sobrenome,cpf,numTel){
    if ((email==null||email=='')||(senhaQuatro==null||senhaQuatro=='')||(senhaOito==null||senhaOito=='')
    ||(nome==null||nome=='')||(sobrenome==null||sobrenome=='')||(cpf==null||cpf=='')
    ||(numTel==null||numTel=='')) {
      alert('existem Campos vazios');
    }
    else{
      Actions.CompCadastro({email,senhaQuatro,senhaOito,nome,sobrenome,cpf,numTel});
    }
    // if ((email==null||email=='')||(senha==null||senha=='')) {
    //   alert('Existem campos vazios');
    // }
    // else{
    //   firebase.auth().createUserAndRetrieveDataWithEmailAndPassword(email, senha).then((user)=>{
    //     console.log(user);
    //     alert('usuário cadstrado');
    //   }).catch(()=>{
    //     console.log('error ao cadastrar usuário');
    //   });
    // }
  }

  render(){
    return (
      <LinearGradient colors={['#A82BB8','#AE2FBF','#F664DA']}  style={styles.container}>
        <HeaderPadrao/>
        <ScrollView style={{flex:1}}>
            <View style={styles.areaLog}>
              <Text style={{fontSize:30,fontWeight:'bold', color:'#fff', marginBottom: 40,marginTop:30}}>Jump City</Text>
              <View style={styles.inpLogin}>
                <TextInput style={styles.textput}
                  placeholder="Digite um email"
                  placeholderTextColor="#fff"
                  onChangeText={(email)=>this.setState({email})}
                />
                <TextInput style={styles.textput}
                  placeholder="Digite uma senha de 8 digítos"
                  placeholderTextColor="#fff"
                  secureTextEntry={true}
                  onChangeText={(senhaOito)=>this.setState({senhaOito})}
                />
                <TextInput style={styles.textput}
                  placeholder="Digite uma senha de 4 digítos"
                  placeholderTextColor="#fff"
                  secureTextEntry={true}
                  onChangeText={(senhaQuatro)=>this.setState({senhaQuatro})}
                />
                <TextInput style={styles.textput}
                  placeholder="Digite Seu Nome"
                  placeholderTextColor="#fff"
                  onChangeText={(nome)=>this.setState({nome})}
                />
                <TextInput style={styles.textput}
                  placeholder="Digite Seu Sobrenome"
                  placeholderTextColor="#fff"
                  onChangeText={(sobrenome)=>this.setState({sobrenome})}
                />
                <TextInput style={styles.textput}
                  placeholder="Digite seu CPF"
                  placeholderTextColor="#fff"
                  onChangeText={(cpf)=>this.setState({cpf})}
                />
                <TextInput style={styles.textput}
                  placeholder="Digite um número de telefone"
                  placeholderTextColor="#fff"
                  onChangeText={(numTel)=>this.setState({numTel})}
                />
                <View style={{width:100,alignSelf:'flex-end',borderRadius:5,marginTop:10}}>
                  <Ripple onPress={()=>this.continueCad(
                    this.state.email,
                    this.state.senhaQuatro,
                    this.state.senhaOito,
                    this.state.nome,
                    this.state.sobrenome,
                    this.state.cpf,
                    this.state.numTel
                  )}>
                    <View style={styles.btnLog}>
                      <Text style={styles.textBtn}>Continuar</Text>
                      <MaterialIcons name="send" size={16} color="#A82BB8" />
                    </View>
                  </Ripple>
                </View>
              </View>
            </View>
        </ScrollView>
      </LinearGradient>
    )
  }
}


const styles=StyleSheet.create({
  container:{
    flex:1
  },
  areaLog:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    paddingBottom:20
  },
  inpLogin:{
    width:'80%',
  },
  btnLog:{
    backgroundColor:'white',
    width:100,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    padding:5,
    borderRadius:5
  },
  textBtn:{
    fontSize:16,
    marginRight:5,
    color: '#A82BB8'
  },
  textput:{
    marginBottom:10,
    height: 40,
    borderColor: '#fff',
    color:'#fff',
    backgroundColor:'rgba(255,255,255, 0.2)',
    paddingHorizontal: 10,
    borderRadius: 5
  }
});
