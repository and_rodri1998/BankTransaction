import React,{ Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  ActivityIndicator
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ripple from 'react-native-material-ripple';
import {Actions} from 'react-native-router-flux';

import firebase from 'react-native-firebase';

import HeaderPadrao from '../../components/headerPadrao';

export default class SingComp extends Component{
  constructor(props){
    super(props);
    this.state={
      endereco:'',
      numCasa:'',
      complemento:'',
      bairro:'',
      load:null
    }
  }
  goInicio(){
    Actions.Inicio({});
  }
  goCadastrar(endereco,numCasa, complemento,bairro){
    this.setState({load:true});
    let database = firebase.database();
    if ((endereco==null||endereco=='')||(numCasa==null||numCasa=='')
        ||(bairro==null||bairro=='')) {
          alert('Existem campos vazios');
          this.setState({load:false});
    }
    else{
      let userLog, uid, userCad;
      firebase.auth().createUserAndRetrieveDataWithEmailAndPassword(this.props.email, this.props.senhaOito).then((user)=>{
        userLog=user;
        uid=user.user.uid;
        this.setState({load:false});
      }).then(()=>{
        let preconta = Math.random()*10;
        let conta=preconta.toFixed(5).replace(".","-"),
        userCad={
          senhaQuatro:this.props.senhaQuatro,
          nome:this.props.nome,
          sobrenome:this.props.sobrenome,
          cpf:this.props.cpf,
          numTel:this.props.numTel,
          endereco:this.state.endereco,
          numCasa:this.state.numCasa,
          complemento:this.state.complemento,
          bairro:this.state.bairro,
          conta:conta,
          agencia:"000001",
          situacaoConta:"ativa"
        },
        situacaoConta={
          saldoatual:"0,00",
          historicoTranfer:{
            ultimo:'0,00'
          },
          historicoDeposito:{
            ultimo:'0,00'
          },
          historicoSenhaQuatro:{
            atual:this.props.senhaQuatro
          }
        };
        firebase.database().ref('users/'+uid).set(userCad).then(()=>{
          firebase.database().ref('contas/'+conta).set(situacaoConta).then(()=>{
            Actions.Inicio({});
          }).catch(()=>{
            console.log('erro ao salvar dados de conta');
          });
        }).catch(()=>{
          console.log('erro ao salvar dados de usuário');
        });
      }).catch((err)=>{
        console.log(err);
        console.log('error ao cadastrar usuário');
      });
    }
  }
  render(){
    let loadComponent=null;
    if (this.state.load) {
      loadComponent=<ActivityIndicator style={{alignSelf:'center', position:'absolute'}} size="small" color="#F664DA" />
    }
    else{loadComponent=null;}
    return (
      <LinearGradient colors={['#A82BB8','#AE2FBF','#F664DA']}  style={styles.container}>
        <HeaderPadrao/>
        <View style={styles.areaLog}>
          <View style={styles.inpLogin}>
            <TextInput TextInput style={styles.textput}
              placeholder="Digite seu endereco"
              placeholderTextColor="#fff"
              onChangeText={(endereco)=>this.setState({endereco})}
            />
            <TextInput TextInput style={styles.textput}
              placeholder="Digite o número da casa"
              placeholderTextColor="#fff"
              onChangeText={(numCasa)=>this.setState({numCasa})}
            />
            {loadComponent}
            <TextInput TextInput style={styles.textput}
              placeholder="Digite o complemento"
              placeholderTextColor="#fff"
              onChangeText={(complemento)=>this.setState({complemento})}
            />
            <TextInput TextInput style={styles.textput}
              placeholder="Digite o bairro"
              placeholderTextColor="#fff"
              onChangeText={(bairro)=>this.setState({bairro})}
            />
            <View style={{width:100,alignSelf:'flex-end',borderRadius:5,marginTop:10}}>
              <Ripple onPress={()=>this.goCadastrar(
                this.state.endereco,
                this.state.numCasa,
                this.state.complemento,
                this.state.bairro
              )}>
                <View style={styles.btnLog}>
                  <MaterialIcons name="lock" size={16} color="#A82BB8" />
                  <Text style={styles.textBtn}>Cadastrar</Text>
                </View>
              </Ripple>
            </View>
          </View>
        </View>
      </LinearGradient>
    )
  }
}


const styles=StyleSheet.create({
  container:{
    flex:1,
  },
  areaLog:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  inpLogin:{
    width:'80%',
  },
  btnLog:{
    backgroundColor:'white',
    width:100,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    padding:5,
    borderRadius:5
  },
  textBtn:{
    fontSize:16,
    marginLeft:5,
    color: '#A82BB8'
  },
  textput:{
    marginBottom:10,
    height: 40,
    borderColor: '#fff',
    color:'#fff',
    backgroundColor:'rgba(255,255,255, 0.2)',
    paddingHorizontal: 10,
    borderRadius: 5
  }
})
