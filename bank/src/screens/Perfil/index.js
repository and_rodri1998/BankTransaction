import React,{ Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TextInput
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import ElevatedView from 'react-native-elevated-view';
import Ripple from 'react-native-material-ripple';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Actions} from 'react-native-router-flux';

import firebase from 'react-native-firebase';

import {conteudoUsuario} from '../../Reducer';

import HeaderPerfil from '../../components/headerPerfil';

class Perfil extends Component{

  constructor(props){
    super(props);
    this.state={email:'',nome:''};
  }

  goSwitch(){
    Actions.SwitchSenha({});
  }

  goExc(){
    Actions.ExcluirConta({});
  }
  buscarDados(){
    let userId = firebase.auth().currentUser.uid,email = firebase.auth().currentUser.email;
    this.setState({email});
    firebase.database().ref('/users/'+userId).once('value').then((snapshot)=>{
      this.props.conteudoUsuario(snapshot.val());
    }).catch((err)=>{
      console.log(err);
      console.log('erro ao localizar conta');
    });
  }
  componentDidMount(){
    this.buscarDados();
  }

  render(){
    let conteudoJSX=null,header=<HeaderPerfil nome="nome"/>;
    if (!this.props.userDados) {
      conteudoJSX=<Text style={styles.textInfoPerfil}>Carregando informações</Text>

    }else{
      let {userDados}=this.props;
      header=<HeaderPerfil nome={userDados.nome}/>;
      conteudoJSX=<View style={{alignItems: 'center'}}><Text style={styles.textInfoPerfil}>{userDados.situacaoConta}</Text>
        <Text style={styles.textInfoPerfil}>{userDados.agencia}</Text>
        <Text style={styles.textInfoPerfil}>{userDados.conta}</Text>
        <Text style={styles.textInfoPerfil}>{userDados.cpf}</Text>
        <Text style={{fontSize:14}}>{this.state.email}</Text>
        <Text style={styles.textInfoPerfil}>{userDados.endereco}, {userDados.numCasa}</Text>
        <Text style={styles.textInfoPerfil}>{userDados.endnumTelereco}</Text>
      </View>
    }

    return (
      <LinearGradient colors={['#A82BB8','#AE2FBF','#F664DA']}  style={styles.container}>
        {header}
        <View style={{flex:1,alignItems:'center',width:'100%'}}>
          <ScrollView style={{padding:10,width:'100%'}}>
            <View style={styles.listOpcoes}>
              <ElevatedView style={styles.cardInfo} elevation={2}>
                {conteudoJSX}
              </ElevatedView>
              <View style={{height:40,width:200}}>
                <Ripple onPress={()=>this.goSwitch()}>
                  <View style={styles.btnActBot}>
                    <Text style={{color: '#A82BB8'}}>Alterar Senhas</Text>
                  </View>
                </Ripple>
              </View>
              <View style={{height:40,width:200,marginTop:10}}>
                <Ripple onPress={()=>this.goExc()}>
                  <View style={styles.btnActBotExc}>
                    <Text style={{color:'#fff'}}>Excluir Conta</Text>
                  </View>
                </Ripple>
              </View>
            </View>
          </ScrollView>
        </View>
      </LinearGradient>
    )
  }
}

const styles= StyleSheet.create({
  container:{
    flex:1
  },
  btnAct:{
    width:200,
    height:40,
    padding:5,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#fff',
    borderRadius:5
  },
  btnActBot:{
    width:200,
    height:40,
    padding:5,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#fff',
    borderRadius:5
  },
  btnActBotExc:{
    width:200,
    height:40,
    padding:5,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#a51c23',
    borderRadius:5
  },
  listOpcoes:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    paddingHorizontal:20
  },
  inpPerf:{
    width:'95%',
    marginTop: 10
  },
  textInfoPerfil:{
    fontSize:20,
    padding:5
  },
  cardInfo:{
    marginBottom:10,
    width:'100%',
    marginTop:10,
    alignItems:'center',
    borderRadius: 5,
    padding:10
  },
  textput:{
    marginBottom:10,
    height: 40,
    borderColor: '#fff',
    color:'#fff',
    backgroundColor:'rgba(255,255,255, 0.2)',
    paddingHorizontal: 10,
    borderRadius: 5
  }
});

const mapStateToProps=(state)=>{
  return {userDados:state.user.userDadosPerfil};
}

const mapDispatchToProps=(dispatch)=>{
  return bindActionCreators({conteudoUsuario},dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(Perfil);
