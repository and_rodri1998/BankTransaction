import React,{ Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  ToastAndroid
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ripple from 'react-native-material-ripple';
import {Actions} from 'react-native-router-flux';
import firebase from 'react-native-firebase';

import HeaderPadrao from '../../components/headerPadrao';

export default class SenhaOito extends Component{
  constructor(props){
    super(props);
    this.state={
      senhaNova:''
    }
  }

  _alterarSenha(senhaNova){
    if (senhaNova==null||senhaNova=='') {
      alert('Existem campos vazios');
    }else{
      let user = firebase.auth().currentUser;

      user.updatePassword(senhaNova).then(function() {
        ToastAndroid.show('Senha alterada com sucesso!', ToastAndroid.SHORT);
        Actions.popTo('Perfil');
      }).catch(function(error) {
        console.log(error);
      });
    }
  }

  goPerf(){
    Actions.popTo('Perfil');
  }

  render(){
    return (
      <LinearGradient colors={['#A82BB8','#AE2FBF','#F664DA']}  style={styles.container}>
        <HeaderPadrao />
        <View style={styles.areaLog}>
          <Text style={{fontSize:20,fontWeight:'bold', color:'#fff', marginBottom: 10}}>Alterar senha de 8 dígitos</Text>
          <View style={styles.inpLogin}>
            <TextInput style={styles.textput}
              placeholder="Nova senha de 8 dígitos"
              placeholderTextColor="#fff"
              secureTextEntry={true}
              onChangeText={(senhaNova)=>this.setState({senhaNova})}
            />
            <View style={{width:100,alignSelf:'flex-end',borderRadius:5,marginTop:10}}>
              <Ripple onPress={()=>this._alterarSenha(this.state.senhaNova)}>
                <View style={styles.btnLog}>
                  <Text style={styles.textBtn}>Alterar</Text>
                </View>
              </Ripple>
            </View>
          </View>
        </View>
      </LinearGradient>
    )
  }
}

const styles=StyleSheet.create({
  container:{
    flex:1,
  },
  areaLog:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  inpLogin:{
    width:'80%',
  },
  btnLog:{
    backgroundColor:'white',
    width:100,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    padding:5,
    borderRadius:5
  },
  textBtn:{
    fontSize:16,
    marginLeft:5,
    color: '#A82BB8'
  },
  textput:{
    marginBottom:10,
    height: 40,
    borderColor: '#fff',
    color:'#fff',
    backgroundColor:'rgba(255,255,255, 0.2)',
    paddingHorizontal: 10,
    borderRadius: 5
  }
})
