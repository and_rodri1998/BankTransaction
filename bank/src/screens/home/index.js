import React,{ Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  ActivityIndicator
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Actions} from 'react-native-router-flux';

import {conteudoContaUsuario} from '../../Reducer';

import ElevatedView from 'react-native-elevated-view';
import Ripple from 'react-native-material-ripple';

import HeaderHome from '../../components/headerHome';

import firebase from 'react-native-firebase';

class Home extends Component{

  constructor(props){
    super(props);
    this.state={currentUser:null};
  }

  goPerf(){
    Actions.Perfil({});
  }
  goDepos(){
    Actions.TelaDeposito({});
  }
  goTransf(){
    Actions.TelaTransferencia({});
  }
  buscarDadosConta(){
    let userId = firebase.auth().currentUser.uid;
    firebase.database().ref('/users/'+userId).once('value').then((snapshot)=>{
      let {conta} = snapshot.val();
      return conta;
    }).then((conta)=>{
      firebase.database().ref('/contas/'+conta).once('value').then((snapshot)=>{
        this.props.conteudoContaUsuario(snapshot.val());
      }).catch((err)=>{
        console.log('erro ao buscar dados de conta');
      });
    }).catch((err)=>{
      console.log(err);
      console.log('erro ao localizar conta');
    });
  }

  componentDidMount() {
    this.buscarDadosConta();
  }

  render(){
    if (this.props.contDados) {
      let {contDados} = this.props;
      return (
        <LinearGradient colors={['#A82BB8','#AE2FBF','#F664DA']}  style={styles.container}>
          <HeaderHome funIcone={this.goPerf}/>
          <View style={styles.containerCorpo}>
            <ScrollView style={{padding:10}}>
              <ElevatedView style={{marginBottom:10, borderRadius:5}} elevation={2}>
                <View style={styles.contCard}>
                  <Text style={styles.labelSaldo}>Saldo atual</Text>
                  <Text style={styles.saldoText}>R$ {contDados.saldoatual}</Text>
                </View>
              </ElevatedView>
              <ElevatedView style={{marginBottom:10, borderRadius:5}} elevation={2}>
                <View style={styles.contCardSecond}>
                  <View style={styles.contDivCard}>
                    <Text style={styles.labelSaldo}>Últ. depósito</Text>
                    <Text style={styles.saldoText}>R$ {contDados.historicoDeposito.ultimo}</Text>
                  </View>
                  <View style={styles.contDivCard}>
                    <Text style={styles.labelSaldo}>Últ. transferência</Text>
                    <Text style={styles.saldoText}>R$ {contDados.historicoTranfer.ultimo}</Text>
                  </View>
                </View>
              </ElevatedView>
              <View style={styles.listOpcoes}>
                <View style={{height:40,width:120,marginRight:10}}>
                  <Ripple onPress={()=>this.goDepos()}>
                    <View style={styles.btnAct}>
                    <MaterialIcons name="attach-money" size={16} color="#A82BB8" />
                      <Text style={{color:'#A82BB8'}}>Depósito</Text>
                    </View>
                  </Ripple>
                </View>
                <View style={{height:40,width:120}}>
                  <Ripple onPress={()=>this.goTransf()}>
                    <View style={styles.btnAct}>
                    <MaterialIcons name="attach-money" size={16} color="#A82BB8" />
                      <Text style={{color:'#A82BB8'}}>Transferência</Text>
                    </View>
                  </Ripple>
                </View>
              </View>
            </ScrollView>
          </View>
        </LinearGradient>
      )
    }
    else{
      return (
        <View style={{flex:1}}>
          <View style={{alignItems:'center',justifyContent:'center',flex:1}}>
            <View>
              <ActivityIndicator size="large" color="#F664DA" />
              <Text style={{color:'#000',fontSize:20}}>Carregando Dados</Text>
            </View>
          </View>
        </View>
      )
    }
  }
}

const styles= StyleSheet.create({
  container:{
    flex:1,
  },
  containerCorpo:{
    flex:1
  },
  contCard:{
    padding:20,
    alignItems:'center',
    justifyContent:'center'
  },
  contCardSecond:{
    padding:20,
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row'
  },
  saldoText:{
    fontSize:30,
    fontWeight:'bold'
  },
  labelSaldo:{
    fontSize:16
  },
  contDivCard:{
    paddingHorizontal:30,
    alignItems:'center',
    justifyContent:'center'
  },
  btnAct:{
    flexDirection: 'row',
    width:120,
    height:40,
    padding:5,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#fff',
    borderRadius:5
  },
  listOpcoes:{
    flexDirection:'row',
    alignSelf:'flex-end'
  }
})

const mapStateToProps=(state)=>{
  return {contDados:state.user.contdados};
}

const mapDispatchToProps=(dispatch)=>{
  return bindActionCreators({conteudoContaUsuario},dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(Home);
