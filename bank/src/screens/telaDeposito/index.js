import React,{ Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ripple from 'react-native-material-ripple';
import {Actions} from 'react-native-router-flux';

import firebase from 'react-native-firebase';
import md5 from 'react-native-md5';

import HeaderPadrao from '../../components/headerPadrao';

export default class TelaDeposito extends Component{
  constructor(props){
    super(props);
    this.state={valor:0,senhafour:''};
  }

  _depositar(valor,senha){
    if ((valor==null||valor=='')||(senha==null||senha=='')) {
      alert('Existem campos vazios');
    }else{
      let prehash=new Date(),deposito={valor,senha,data:prehash};
      let hash=md5.hex_md5(prehash)+"=deposito";

      let userId = firebase.auth().currentUser.uid;
      firebase.database().ref('/users/'+userId).once('value').then((snapshot)=>{
        let {conta,senhaQuatro} = snapshot.val();
        if (senhaQuatro==senha) {
          return conta;
        }else{
          return null;
        }
      }).then((conta)=>{
        if (conta) {
          firebase.database().ref('/contas/'+conta+'/historicoDeposito/'+hash).set(deposito).then(()=>{
            // Actions.Inicio({});
            firebase.database().ref('/contas/'+conta+'/historicoDeposito/ultimo').set(deposito.valor).then(()=>{
              return deposito.valor;
            }).then((valor)=>{
              firebase.database().ref('/contas/'+conta).once('value').then((snapshot)=>{
                let {saldoatual} = snapshot.val();
                return saldoatual;
              }).then((saldoatual)=>{
                let saldo=parseFloat(valor)+parseFloat(saldoatual);
                firebase.database().ref('/contas/'+conta+'/saldoatual').set(saldo.toFixed(2).replace(".",",")).then(()=>{
                  console.log('valor depositado');
                  Actions.Inicio({});
                })
              })
            }).catch(()=>{
              console.log('erro ao depositar');
            });
          });
        }else{
            alert('Senha incorreta');
        }
      }).catch((err)=>{
        console.log(err);
        console.log('erro ao localizar conta');
      });
    }
  }

  goInicio(){
    Actions.popTo('Inicio');
  }

  render(){
    return (
      <LinearGradient colors={['#A82BB8','#AE2FBF','#F664DA']}  style={styles.container}>
        <HeaderPadrao />
        <View style={styles.areaLog}>
          <Text style={{fontSize:20,fontWeight:'bold', color:'#fff'}}>Depósito</Text>
          <View style={styles.inpLogin}>
            <TextInput style={styles.textput}
              placeholder="Valor"
              placeholderTextColor="#fff"
              onChangeText={(valor)=>{this.setState({valor})}}
            />
            <TextInput style={styles.textput}
              placeholder="Digite sua senha de 4 digítos"
              placeholderTextColor="#fff"
              secureTextEntry={true}
              onChangeText={(senhafour)=>{this.setState({senhafour})}}
            />
            <View style={{width:100,alignSelf:'flex-end',borderRadius:5,marginTop:10}}>
              <Ripple onPress={()=>this._depositar(this.state.valor, this.state.senhafour)}>
                <View style={styles.btnLog}>
                  <MaterialIcons name="attach-money" size={16} color="#A82BB8" />
                  <Text style={styles.textBtn}>Depositar</Text>
                </View>
              </Ripple>
            </View>
          </View>
        </View>
      </LinearGradient>
    )
  }
}

const styles=StyleSheet.create({
  container:{
    flex:1,
  },
  areaLog:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  inpLogin:{
    width:'80%',
  },
  btnLog:{
    backgroundColor:'white',
    width:100,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    padding:5,
    borderRadius:5
  },
  textBtn:{
    fontSize:16,
    color: '#A82BB8'
  },
  textput:{
    marginBottom:10,
    height: 40,
    borderColor: '#fff',
    color:'#fff',
    backgroundColor:'rgba(255,255,255, 0.2)',
    paddingHorizontal: 10,
    borderRadius: 5
  }
})
