import React,{ Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ripple from 'react-native-material-ripple';
import {Actions} from 'react-native-router-flux';
import firebase from 'react-native-firebase';
import md5 from 'react-native-md5';

import HeaderPadrao from '../../components/headerPadrao';

export default class TelaTransferencia extends Component{
  constructor(props){
    super(props);
    this.state={contadestino:'',valor:'',senha:''};
  }

  goInicio(){
    Actions.popTo('Inicio');
  }

  _transferir(contadestino,valor,senha){
    if ((valor==null||valor=='')||(senha==null||senha=='')||(contadestino==null||contadestino=='')) {
      alert('Existem campos vazios');
    }else{
      let prehash=new Date(),transferencia={contaremetente:'',valor,contadestino,data:prehash};
      let hash=md5.hex_md5(prehash)+"=transferencia";

      let userId = firebase.auth().currentUser.uid;
      firebase.database().ref('/users/'+userId).once('value').then((snapshot)=>{
        let {conta,senhaQuatro} = snapshot.val();
        if (senhaQuatro==senha){
          return conta;
        }else{
          return null;
        }
      }).then((conta)=>{
        if (conta) {
          transferencia.contaremetente=conta;
          firebase.database().ref('/contas/'+conta).once('value').then((snapshot)=>{
            let {saldoatual} = snapshot.val();
            if (parseFloat(saldoatual)>parseFloat(valor)) {
              return saldoatual;
            }else{
              return null;
            }
          }).then((saldoatual)=>{
            if (saldoatual) {
              firebase.database().ref('/contas/'+conta+'/historicoTranfer/enviado/'+hash).set(transferencia).then(()=>{
                firebase.database().ref('/contas/'+conta+'/historicoTranfer/ultimo').set(transferencia.valor).then(()=>{
                  return transferencia.valor;
                }).then((valor)=>{

                  let saldo=parseFloat(saldoatual)-parseFloat(valor);
                  firebase.database().ref('/contas/'+conta+'/saldoatual').set(saldo.toFixed(2).replace(".",",")).then(()=>{
                    console.log('valor enviado');
                  }).then(()=>{
                    firebase.database().ref('/contas/'+transferencia.contadestino+'/historicoTranfer/recebido/'+hash).set(transferencia).then(()=>{

                      firebase.database().ref('/contas/'+transferencia.contadestino).once('value').then((snapshot)=>{
                        let {saldoatual} = snapshot.val();
                        return saldoatual;
                      }).then((saldoatual)=>{
                        let saldo=parseFloat(valor)+parseFloat(saldoatual);
                        firebase.database().ref('/contas/'+transferencia.contadestino+'/saldoatual').set(saldo.toFixed(2).replace(".",",")).then(()=>{
                          console.log('valor recebido');
                          Actions.Inicio({});
                        })
                      });

                    });
                  });

                }).catch(()=>{
                  console.log('erro ao depositar');
                });
              }).catch((err)=>{
                console.log(err);
              });
            }else{
              alert('Acho que você está sem dinheiro, isto está certo?');
            }
          });
        }else{
            alert('Senha incorreta');
        }
      }).catch((err)=>{
        console.log(err);
        console.log('erro ao localizar conta');
      });
    }
  }

  render(){
    return (
      <LinearGradient colors={['#A82BB8','#AE2FBF','#F664DA']}  style={styles.container}>
        <HeaderPadrao />
        <View style={styles.areaLog}>
          <Text style={{fontSize:20,fontWeight:'bold', color:'#fff'}}>Transferência</Text>
          <View style={styles.inpLogin}>
            <TextInput style={styles.textput}
              placeholder="Conta"
              placeholderTextColor="#fff"
              onChangeText={(contadestino)=>{this.setState({contadestino})}}
            />
            <TextInput style={styles.textput}
              placeholder="Valor"
              placeholderTextColor="#fff"
              onChangeText={(valor)=>{this.setState({valor})}}
            />
            <TextInput style={styles.textput}
              placeholder="Digite sua senha de 4 digítos"
              placeholderTextColor="#fff"
              secureTextEntry={true}
              onChangeText={(senha)=>{this.setState({senha})}}
            />
            <View style={{width:100,alignSelf:'flex-end',borderRadius:5,marginTop:10}}>
              <Ripple onPress={()=>this._transferir(this.state.contadestino,this.state.valor,this.state.senha)}>
                <View style={styles.btnLog}>
                  <MaterialIcons name="attach-money" size={16} color="#A82BB8" />
                  <Text style={styles.textBtn}>Transferir</Text>
                </View>
              </Ripple>
            </View>
          </View>
        </View>
      </LinearGradient>
    )
  }
}

const styles=StyleSheet.create({
  container:{
    flex:1,
  },
  areaLog:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  inpLogin:{
    width:'80%',
  },
  btnLog:{
    backgroundColor:'white',
    width:100,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    padding:5,
    borderRadius:5
  },
  textBtn:{
    fontSize:16,
    color: '#A82BB8'
  },
  textput:{
    marginBottom:10,
    height: 40,
    borderColor: '#fff',
    color:'#fff',
    backgroundColor:'rgba(255,255,255, 0.2)',
    paddingHorizontal: 10,
    borderRadius: 5
  }
})
