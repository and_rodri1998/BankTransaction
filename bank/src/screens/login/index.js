import React,{ Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  ActivityIndicator
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Actions} from 'react-native-router-flux';

import firebase from 'react-native-firebase';

import {fetchPosts, logarWithRedux, verificarLogado} from '../../Reducer';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ripple from 'react-native-material-ripple';

import HeaderLogin from '../../components/headerLog';

class Login extends Component{
  constructor(props){
    super(props);
    this.state={email:null,senha:null,load:false};
  }

  componentDidMount(){
    // chama action creator para pegar dados
    this.props.verificarLogado();
  }

  goInicio(){
    Actions.Inicio({});
  }

  goCadastro(){
    Actions.Cadastro({});
  }

  _logar(email, senha){
    if((email==null||email=="")||(senha==null||senha=="")){
      alert('Campo de usuário ou senha está vazio');
    }else{
      this.setState({load:true});
      let isErr=false;
      firebase.auth().signInAndRetrieveDataWithEmailAndPassword(email, senha).then((user)=>{
        this.props.logarWithRedux(user);
        let clearInter=setInterval(()=>{
          if (this.props.userDados) {
            this.setState({load:false});
            clearInterval(clearInter);
            this.goInicio();
          }
        });
      }).catch((error)=> {
        // var errorCode = error.code;
        this.setState({load:false});
        let errorMessage = error.message;
        isErr=true;
        alert('senha ou email errados');
        console.log('senha ou email errados');
      });
      // if (!isErr) {
      //   let clearExt=setInterval(()=>{
      //     if(userLog){
      //       clearInterval(clearExt);
      //
      //     }
      //   },1000);
      // }
    }
  }

  render(){
    let loadComponent=null;
    if (this.state.load) {
      loadComponent=<ActivityIndicator style={{alignSelf:'center', position:'absolute'}} size="small" color="#F664DA" />
    }
    else{loadComponent=null;}
    return (
      <LinearGradient colors={['#A82BB8','#AE2FBF','#F664DA']}  style={styles.container}>
        <HeaderLogin singUp={this.goCadastro} />
        <View style={styles.areaLog}>
          <Text style={{fontSize:30,fontWeight:'bold', color:'#fff', marginBottom: 40}}>Jump City</Text>
          <View style={styles.inpLogin}>
            <TextInput style={styles.textput}
              placeholder="Digite Seu email"
              placeholderTextColor="#fff"
              onChangeText={(emailinp) => this.setState({email:emailinp})}
            />
            {loadComponent}
            <TextInput style={styles.textput}
              placeholder="Digite sua senha de 8 digítos"
              placeholderTextColor="#fff"
              secureTextEntry={true}
              onChangeText={(senhaInp) => this.setState({senha:senhaInp})}
            />
            <View style={{width:100,alignSelf:'flex-end',borderRadius:5,marginTop:10}}>
              <Ripple onPress={()=>this._logar(this.state.email,this.state.senha)}>
                <View style={styles.btnLog}>
                  <MaterialIcons name="lock" size={16} color="#A82BB8" />
                  <Text style={styles.textBtn}>Entrar</Text>
                </View>
              </Ripple>
            </View>
          </View>
        </View>
      </LinearGradient>
    );
  }
}

const styles=StyleSheet.create({
  container:{
    flex:1,
  },
  areaLog:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  inpLogin:{
    width:'80%',
  },
  btnLog:{
    backgroundColor:'white',
    width:100,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    padding:5,
    borderRadius:5
  },
  textBtn:{
    fontSize:16,
    marginLeft:5,
    color: '#A82BB8'
  },
  textput:{
    marginBottom:10,
    height: 40,
    borderColor: '#fff',
    color:'#fff',
    backgroundColor:'rgba(255,255,255, 0.2)',
    paddingHorizontal: 10,
    borderRadius: 5
  }
});

const mapStateToProps=(state)=>{
  return {userDados:state.user.userDados};
}

const mapDispatchToProps=(dispatch)=>{
  return bindActionCreators({fetchPosts, logarWithRedux,verificarLogado},dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(Login);

// createUserAndRetrieveDataWithEmailAndPassword(email, password) returns Promise containing UserCredential;
// signInAndRetrieveDataWithEmailAndPassword(email, password) returns Promise containing UserCredential;
