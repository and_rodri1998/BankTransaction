import React,{ Component } from 'react';
import {
  Text,
  View,
  StyleSheet
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {Actions} from 'react-native-router-flux';
import Ripple from 'react-native-material-ripple';

import HeaderPadrao from '../../components/headerPadrao';

export default class SwitchSenha extends Component{
  goFour(){
    Actions.SenhaQuatro({})
  }
  goOito(){
    Actions.SenhaOito({})
  }
  render(){
    return (
      <LinearGradient colors={['#A82BB8','#AE2FBF','#F664DA']}  style={styles.container}>
        <HeaderPadrao />
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
          <Text style={{fontSize:20,fontWeight:'bold',marginBottom:10, color:'#fff'}}>Alterar Senha de:</Text>
          <View style={{height:40,width:200,marginBottom:10}}>
            <Ripple onPress={()=>this.goFour()}>
              <View style={styles.btnAct}>
                <Text style={{color: '#A82BB8'}}>4 dígitos</Text>
              </View>
            </Ripple>
          </View>
          <View style={{height:40,width:200}}>
            <Ripple onPress={()=>this.goOito()}>
              <View style={styles.btnAct}>
                <Text style={{color: '#A82BB8'}}>8 dígitos</Text>
              </View>
            </Ripple>
          </View>
        </View>
      </LinearGradient>
    )
  }
}

const styles= StyleSheet.create({
  container:{
    flex:1
  },
  btnAct:{
    width:200,
    height:40,
    padding:5,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#fff',
    borderRadius:5
  },
})
