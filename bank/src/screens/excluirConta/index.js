import React,{ Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  ToastAndroid
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ripple from 'react-native-material-ripple';
import {Actions} from 'react-native-router-flux';
import firebase from 'react-native-firebase';

import HeaderPadrao from '../../components/headerPadrao';

export default class ExcluirConta extends Component{
  constructor(props){
    super(props);
    this.state={
      email:'',
      senha:''
    }
  }

  _excluirConta(email,senha){
    if ((email==null||email=='')||(senha==''||senha==null)) {
      alert('Existem campos vazios');
    }else{
      var user = firebase.auth().currentUser;

      if (user.email==email) {
        firebase.database().ref('/users/'+user.uid).once('value').then((snapshot)=>{
          let {senhaQuatro,conta} = snapshot.val();
          if (senhaQuatro==senha) {
            firebase.database().ref('/users/'+user.uid).set(null).then(()=>{
              firebase.database().ref('/contas/'+conta).set(null).then(()=>{
                user.delete().then(()=>{
                  ToastAndroid.show('Conta excluida sucesso!', ToastAndroid.SHORT);
                  Actions.Login({});
                }).catch(function(error) {
                  console.log(error)
                });
              }).catch((err)=>{
                console.log(err);
              });
            }).catch((err)=>{
              console.log(err);
            });
          }else{
            alert('Senha antiga não confere');
          }
        }).catch((err)=>{
          console.log(err);
        });
      }else{
        alert('Email incorreto')
      }
    }
  }

  goLogin(){
    Actions.Login({});
  }

  render(){
    return (
      <LinearGradient colors={['#A82BB8','#AE2FBF','#F664DA']}  style={styles.container}>
        <HeaderPadrao />
        <View style={styles.areaLog}>
          <Text style={{fontSize:20,fontWeight:'bold', color:'#fff', marginBottom: 10}}>Excluir conta</Text>
          <View style={styles.inpLogin}>
            <TextInput style={styles.textput}
              placeholder="Digite seu email"
              placeholderTextColor="#fff"
              onChangeText={(email)=>this.setState({email})}
            />
            <TextInput style={styles.textput}
              placeholder="senha de 4 dígitos"
              placeholderTextColor="#fff"
              secureTextEntry={true}
              onChangeText={(senha)=>this.setState({senha})}
            />
            <View style={{width:'100%',alignSelf:'flex-end',borderRadius:5,marginTop:10}}>
              <Ripple onPress={()=>this._excluirConta(this.state.email, this.state.senha)}>
                <View style={styles.btnLog}>
                  <Text style={styles.textBtn}>Excluir Conta</Text>
                </View>
              </Ripple>
            </View>
          </View>
        </View>
      </LinearGradient>
    )
  }
}

const styles=StyleSheet.create({
  container:{
    flex:1,
  },
  areaLog:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  inpLogin:{
    width:'80%',
  },
  btnLog:{
    backgroundColor:'#a51c23',
    width:'100%',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    padding:5,
    borderRadius:5
  },
  textBtn:{
    fontSize:16,
    color:'#fff'
  },
  textput:{
    marginBottom:10,
    height: 40,
    borderColor: '#fff',
    color:'#fff',
    backgroundColor:'rgba(255,255,255, 0.2)',
    paddingHorizontal: 10,
    borderRadius: 5
  }
})
