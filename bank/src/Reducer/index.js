import axios from 'axios';
import { combineReducers} from 'redux';
import firebase from 'react-native-firebase';

// CONSTS DE ACTION
const TESTE='TESTE';
const LOGAR='LOGAR';
const VERIFICAR_LOG='VERIFICAR_LOG';
const LOGOUT='LOGOUT';
const CADASTRAR='CADASTRAR';
const DADOSUSERCONTA='DADOSUSERCONTA';
const DADOSUSER='DADOSUSER';

// REDUCERS
const initial_state= {
  all:[],
  selected:null,
  user:null,
  userDados:null,
  contdados:null,
  userDadosPerfil:null
}

const reducerUserDados= (state=initial_state,action)=>{
  switch (action.type) {
    case TESTE:
      return {
        ...state,
        all:action.payload
      }
      break;
    case LOGAR:
      return {
        ...state,
        userDados:action.payload
      }
      break;
    case CADASTRAR:
      return {
        ...state,
        userDados:action.payload
      }
      break;
    case VERIFICAR_LOG:
      return {
        ...state,
        userDados:action.payload
      }
      break;
    case LOGOUT:
      return {
        ...state,
        userDados:action.payload
      }
      break;
    case DADOSUSER:
      return {
        ...state,
        userDadosPerfil:action.payload
      }
      break;
    case DADOSUSERCONTA:
      return {
        ...state,
        contdados:action.payload
      }
    default:
      return state;
  }
}

// retorna todos os reducers combinados
const reducers= combineReducers({
  user:reducerUserDados
});

export default reducers;

// ACTIONS
export const fetchPosts=()=>{
  // fazer pedido http e retornar dados
    return {
      type:TESTE,
      payload:"teste data"
    }
};

export const logarWithRedux= (user)=>{
  try{
      return {
        type:LOGAR,
        payload:user
      }
  }catch(err){
      console.log(err);
      alert('usuario ou senha incorretos');
  }
}
export const verificarLogado=async ()=>{
  try{
    let currentUser=null;
      await firebase.auth().onAuthStateChanged(function(user){
          if (user) {
            currentUser=user;
          }
      });
      return {
        type:VERIFICAR_LOG,
        payload:currentUser
      }
  }catch(err){
      console.log(err);
  }
}

export const singOut=()=>{
    return {
      type:LOGOUT,
      payload:null
    }
}

export const cadastrar=(user)=>{
    return {
      type:CADASTRAR,
      payload:user
    }
}
export const conteudoContaUsuario=(dadosConta)=>{
  return {
    type:DADOSUSERCONTA,
    payload:dadosConta
  }
}
export const conteudoUsuario=(dadosUser)=>{
  return {
    type:DADOSUSER,
    payload:dadosUser
  }
}
